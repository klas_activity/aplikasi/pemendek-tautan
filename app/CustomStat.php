<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomStat extends Model
{
    protected $table = "stats_custom_url";

    protected $fillable = [
        'custom_id', 'created_at', 'updated_at'
    ];
}
