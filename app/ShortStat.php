<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShortStat extends Model
{
    protected $table = "stats_short_url";

    protected $fillable = [
        'short_id', 'created_at', 'updated_at'
    ];
}
