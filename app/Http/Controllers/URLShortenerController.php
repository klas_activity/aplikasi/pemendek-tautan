<?php
namespace App\Http\Controllers;

use App\CustomStat;
use App\CustomUrl;
use App\DataStatistik;
use App\ShortStat;
use App\ShortUrl;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Crypt;
use Illuminate\View\View;

class URLShortenerController extends Controller
{
    /**
     * Index
     */
    public function index() {
        return view('pages/home');
    }

    public function index_new() {
        return view('pages/home-new');
    }

    /**
     * Decrypt the string
     *
     * @param $string
     * @return mixed
     */
    private function decrypt($string) {
        return Crypt::decryptString($string);
    }

    /**
     * Encrypt the string
     *
     * @param $string
     * @return mixed
     */
    private function encrypt($string) {
        return Crypt::encryptString($string);
    }

    /**
     * Remove URL Pattern like http://domain.com
     * Also remove unwanted char like '.' and '-'
     *
     * @param $string
     * @return string|string[]|null
     */
    private function removeUnwantedString($string) {
        $parse = parse_url($string);
        if (isset($parse['scheme'])) {
            $string = str_replace(array($parse['scheme'], "://", $parse['host']), "", $string);
        }
        $cleanString = preg_replace("/[^a-zA-Z0-9 \. \-]/", "", $string);
        return $cleanString;
    }

    /**
     * Check if the URL exist
     *
     * @param $url
     * @return array
     */
    private function findUrl($url) {
        if ($url != null || $url != "") {
            $shorturl_q = ShortUrl::all();
            if (count($shorturl_q) != 0) {
                foreach ($shorturl_q as $item) {
                    if ($this->decrypt($item->url) == $url) {
                        return ["found" => true, "id" => $item->id, "shorturl" => $item->shorturl];
                    }
                }
            }
        }
        return ["found" => false, "id" => 0, "shorturl" => ""];
    }

    /**
     * Check if the custom string that is use in custom URL already exist or not
     *
     * @param $string
     * @return array
     */
    private function findCustomUrl($customurl) {
        if ($customurl != null || $customurl != "") {
            $customurl_q = CustomUrl::all();
            if (count($customurl_q) != 0) {
                foreach ($customurl_q as $item) {
                    if ($this->decrypt($item->customurl) == $customurl) {
                        return ["found" => true, "id" => $item->id, "url_id" => $item->url_id, "customurl" => $item->customurl];
                    }
                }
            }
        }
        return ["found" => false, "id" => 0, "url_id" => 0, "customurl" => ""];
    }

    /**
     * Generate Random String with default length = 3
     *
     * @param int $length
     * @return string
     */
    protected function randomString($length = 3)
    {
        $str = "";
        $characters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }

    /**
     * Generate Random String that not used yet
     *
     * @return string
     */
    private function generateUnexistRandomString() {
        $data = ShortUrl::all();
        $rand_string = $this->randomString();
        foreach ($data as $item) {
            if ($this->decrypt($item->shorturl) == $rand_string) {
                $this->generateUnexistRandomString();
                break;
            }
        }
        return $rand_string;
    }

    private function incrementStatistic($column) {
        try {
            $stat = DataStatistik::query()->where('nama', '=', $column)->firstOrFail();
            $stat->update([
                'nama' => $stat->nama,
                'nilai' => $stat->nilai + 1
            ]);
        } catch (QueryException $queryException) {
            return false;
        }
        return true;
    }

    /**
     * Build short url
     *
     * @param Request $request
     * @return Factory|View|JsonResponse|Redirector
     */
    public function doShort(Request $request)
    {
        $token = $request->get('_token');
        $url = $request->get('url');
        $customurl = $request->get('customurl');

        if ($url != "" && $url != null) {
            $url_check = $this->findUrl($url);
            if (!$url_check['found']) {
                $newShortUrl = new ShortUrl;
                $newShortUrl->url = $this->encrypt($url);
                $newShortUrl->shorturl = $this->encrypt($this->generateUnexistRandomString());
                $newShortUrl->save();
                $url_check['found'] = true;
                $url_check['id'] = $newShortUrl->id;
                $url_check['shorturl'] = $newShortUrl->shorturl;
                $this->incrementStatistic('shortlinkgenerate');
            }
        } else {
            if ($token != "") {
                return redirect(route('home'));
            } else {
                return response()->json([
                    "error" => true,
                    "message" => "Need url, shorturl, and customurl parameter!",
                    "data" => [
                        "url" => "",
                        "shorturl" => "",
                        "customurl" => ""
                    ]
                ]);
            }
        }

        if ($customurl != "" && $customurl != null) {
            $customurl = $this->removeUnwantedString($customurl);

            if ($customurl == "home" || $customurl == "login" || $customurl == "register") {
                if ($token != "") {
                    return view('pages/home')->with("error", "Tautan kustom tidak dapat digunakan!");
                } else {
                    return response()->json([
                        "error" => true,
                        "message" => "Tautan kustom tidak dapat digunakan!",
                        "data" => [
                            "url" => $url,
                            "customurl" => $customurl
                        ]
                    ]);
                }
            }

            $customurl_check = $this->findCustomUrl($customurl);

            if (!$customurl_check['found']) {
                $newCustomUrl = new CustomUrl;
                $newCustomUrl->url_id = $url_check['id'];
                $newCustomUrl->customurl = $this->encrypt($customurl);
                $newCustomUrl->save();
                $customurl_check['found'] = true;
                $customurl_check['url_id'] = $newCustomUrl->url_id;
                $customurl_check['id'] = $newCustomUrl->id;
                $customurl_check['customurl'] = $newCustomUrl->customurl;
                $this->incrementStatistic('shortlinkcustom');
            } else {
                if ($customurl_check['url_id'] != $url_check['id']) {
                    if ($token != "") {
                        return view('pages/home')
                        ->with("error", "Tautan kustom telah digunakan!")
                        ->with("customurl", $customurl);
                    } else {
                        return response()->json([
                            "error" => true,
                            "message" => "Tautan kustom telah digunakan!",
                            "data" => [
                                "url" => $url,
                                "customurl" => $customurl
                            ]
                        ]);
                    }
                }
            }
        }

        if ($token != "") {
            return view('pages/result', ['result' => [
                'url' => $url,
                'shorturl' => $this->decrypt($url_check['shorturl']),
                'customurl' => isset($customurl_check) ? $this->decrypt($customurl_check['customurl']) : ""
            ]]);
        } else {
            return response()->json([
                "error" => false,
                "message" => "success",
                "data" => [
                    "url" => $url,
                    "shorturl" => $this->decrypt($url_check['shorturl']),
                    "customurl" => isset($customurl_check) ? $this->decrypt($customurl_check['customurl']) : ""
                ]
            ]);
        }
    }

    /**
     * Redirect to the URL
     *
     * @param $shorturl
     * @return Factory|View
     */
    private function go($shorturl)
    {
        $url = "";
        $short_urls = ShortUrl::with('custom_url')->get();
        $model = 0;
        $isCustomURL = false;
        foreach ($short_urls as $item) {
            foreach ($item->custom_url as $cus_item) {
                if ($this->decrypt($cus_item->customurl) == $shorturl) {
                    $url = $this->decrypt($item->url);
                    $model = $cus_item;
                    $isCustomURL = true;
                    break 2;
                }
            }
            if ($this->decrypt($item->shorturl) == $shorturl) {
                $url = $this->decrypt($item->url);
                $model = $item;
                break;
            }
        }
        if ($url == "") {
            return view('pages/home')->with("error", "Tautan pendek yang anda masukkan tidak ditemukan. (Err: Not Found)");
        }

//        check that the url is not go to this app itself
        if (parse_url($url)['host'] == "s.klas.or.id") {
            return view('pages/home')->with("error", "Tautan pendek yang anda masukkan tidak ditemukan. (Err: Loop)");
        }

        if ($url != "") {
            $this->incrementStatistic('shortlinkakses');
        }
        //add statistic URL
        if ($isCustomURL) {
            CustomStat::query()->create([
                'custom_id' => $model->id
            ]);
        } else {
            ShortStat::query()->create([
                'short_id' => $model->id
            ]);
        }
        return $url;
    }

    /*
    * Adding Meta retrieval service
    *
    */
    private function curl($url, $timeout)
    {
        $ch = curl_init();
        $fake_user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7) Gecko/20040803 Firefox/0.9.3';
        curl_setopt($ch, CURLOPT_USERAGENT, $fake_user_agent); 
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

    private function get_website_title($url, $tags = array ('og:title', 'og:image', 'og:description', 'og:site_name'), $timeout = 10) {
        $html = $this->curl($url, $timeout);
        if (!$html) {
            return false;
        }
        $doc = new \DOMDocument();
        @$doc->loadHTML($html);
        $nodes = $doc->getElementsByTagName('title');
        $ary = [];
        
        $ary['title'] = $nodes->item(0)->nodeValue;
        $metas = $doc->getElementsByTagName('meta');
        
        for ($i = 0; $i < $metas->length; $i++) {
            $meta = $metas->item($i);
            
            foreach($tags as $tag) {
                if ($meta->getAttribute('property') == $tag) {
                    $ary[$tag] = $meta->getAttribute('content');
                }
            }
        }
        return $ary;
    }

    /**
     * @param $shorturl
     * @return Factory|View
     */
    public function go_web($shorturl) {
        $url = $this->go($shorturl);
        return view('pages/redirect', ['url' => $url]);
    }

    public function go_web_new($shorturl) {
        try {
            $url = $this->go($shorturl);
            $arr1 = get_meta_tags($url);
            $arr2 = $this->get_website_title($url);
            $arr = array_merge($arr1, $arr2);
            $arr['klasurl'] = $url;
        } catch (\Exception $err) {
            return view('pages/home-new')->with("error", "Tautan pendek yang anda masukkan tidak ditemukan. (Err: Not Found)");
        }
        return view('pages/redirect-new', ['klasarr' => $arr]);
    }

    /**
     * @param $shorturl
     * @return JsonResponse
     */
    public function go_api($shorturl) {
        $url = $this->go($shorturl);
        return response()->json([
            "error" => false,
            "message" => "success",
            "data" => [
                "url" => $url,
            ]
        ]);
    }

    public function doBackup(Request $request) {
        // if ($request->get('withCustom')) {
        //     $short_urls = ShortUrl::with('custom_url')->get();
        // } else {
        //     $short_urls = ShortUrl::all();
        // }

        // $short_urls = ShortUrl::all();
        $short_urls = ShortUrl::with('custom_url')->get();
        // return $short_urls;

        // $result = [];

        // foreach ($short_urls as $item) {
        //     array_push($result, $item);
        // }

        $result = $short_urls->map(function ($value) {
            return [
                "creator" => null,
                "url" => $this->decrypt($value->url),
                "path" => $this->decrypt($value->shorturl),
                "customurl" => $value->custom_url == null ? [] : $value->custom_url->map(function ($v) {
                    return [
                        "customurl" => $this->decrypt($v->customurl),
                        "created_at" => $v->created_at,
                        "updated_at" => $v->updated_at
                    ];
                }),
                "created_at" => $value->created_at,
                "updated_at" => $value->updated_at
            ];
        });

        return $result;
    }
}
