<?php

namespace App\Http\Controllers;

use App\CustomStat;
use App\ShortStat;

class StatsController extends Controller
{
    private function optimize_data_chart($arr) {
        $data = array();
        foreach ($arr as $item) {
            if (array_key_exists(date_format($item->created_at, 'd/m/Y'), $data)) {
                $data[date_format($item->created_at, 'd/m/Y')]++;
            } else {
                $data[date_format($item->created_at, 'd/m/Y')] = 1;
            }
        }
        ksort($data);
        return $data;
    }

    public function short_stats($id) {
        try {
            $data = ShortStat::query()->where('short_id', $id)->get();
        } catch (\Exception $exception) {
            $data = [];
        }
        return response()->json($this->optimize_data_chart($data));
    }


    public function custom_stats($id) {
        try {
            $data = CustomStat::query()->where('custom_id', $id)->get();
        } catch (\Exception $exception) {
            $data = [];
        }
        return response()->json($this->optimize_data_chart($data));
    }
}
