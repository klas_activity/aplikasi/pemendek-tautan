<p align="center"><img src="public/img/klas.webp"></p>

<p align="center">
<a href="https://klas.or.id">Home |</a>
<a href="https://klas.or.id/index.php/topik/berita/">Berita |</a>
<a href="https://klas.or.id/index.php/topik/events/">Event |</a>
<a href="https://klas.or.id/index.php/topik/events/cangkruan/">Cangkrukan |</a>
<a href="https://klas.or.id/index.php/topik/tutorial/">Tutorial</a>
</p>

## Tentang KLAS

Kelompok Linux Arek Suroboyo adalah tempat berkumpul, berbagi informasi, dan belajar bagi para pengguna Linux dan dan penggiat open source di Surabaya.

## Tentang klas-url-shortener

klas-url-shortener adalah Pemendek Tautan Sederhana dan Cepat dibuat oleh Kelompok Linux Arek Suroboyo.

## Requirement Install klas-url-shortener

KLAS URL Shortener ini berjalan menggunakan platform Laravel. Untuk menjalankan aplikasi ini, diperlukan environment yang sudah terinstall Laravel dan database MySQL. Selain itu, beberapa ekstensi PHP harus dipasang untuk mendukung beberapa fitur penting pada KLAS Url Shortener. Ekstensi tesebut antara lain:

<ul>
<li>php-imagick</li>
<li>php-curl</li>
<li>php-xml</li>
<li>php-gd</li>
<li>php-mbstring</li>
<li>php-mysql</li>
</ul>

## Menjalankan aplikasi URL Shortener

Untuk menjalankan aplikasi URL shortener ini, yang harus dilakukan pertama kali adalah melakukan instalasi library pada composer dan npm. Kemudian melakukan modifikasi pada file .env untuk mendefinisikan nama aplikasi dan Database yang akan dipakai. Setelah itu melakukan Inisialisasi aplikasi menggunakan PHP Artisan.

-   php artisan key:generate
-   php artisan migrate
-   php artisan db:seed

Setelah itu mengakses aplikasi URL shortener menggunakan link http://localhost:8000

## Cara Clone klas-url-shortener

<ul>
<li>Klik button clone /download</li>
<li>copy url</li>
<li>buka terminal, tulis perintah git clone (url yang sudah di copy)</li>
</ul>

Sehingga untuk melakukan cloning repositor ini, perintah yang dimasukkan adalah `git clone https://gitlab.com/klas_activity/aplikasi/pemendek-tautan`

## Kunjungi Kami

<ul style="list-style-type: none; display: inline;">
<li><a href="https://klas.or.id">Website</a></li>
<li><a href="https://www.facebook.com/kelompoklinuxareksuroboyo/">Facebook</a></li>
</ul>

## Lisensi

Lisensi dari project KLAS URL Shortener adalah `MIT`
