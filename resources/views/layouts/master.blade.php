<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta name="description" content="Alat Pemendek Tautan yang dibuat dan dirawat oleh KLAS (Kelompok Linux Arek Suroboyo)."/>

    <link rel="icon" href="{{ asset('img/logo-32x32.png') }}" sizes="32x32">
    <title>{{ __('URL Shortener by Kelompok Linux Arek Suroboyo') }}</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body id="app">
<header class="fixed-top">
    <nav class="navbar navbar-expand-lg navbar-light bg-light hide-on-lg-up">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <a class="navbar-brand" href="https://klas.or.id"><img height="54px" width="auto" alt="KLAS LOGO" src="{{
        asset('img/logo.png') }}"></a>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item"><a class="nav-link" href="https://github.com/cangkrukan-klas/klas-url-shortener"><i class="fab fa-github"></i>{{ __('GitHub Repository') }}</a></li>
                <li class="nav-item"><a class="nav-link" href="https://klas.or.id">{{ __('About Us') }}</a></li>
                <li class="nav-item"><a class="nav-link disabled" href="#">{{ __('Short link created') }}: {{ (\App\DataStatistik::query()->where('nama', 'shortlinkgenerate')->first()->nilai) + (\App\DataStatistik::query()->where('nama', 'shortlinkcustom')->first()->nilai) }}</a> Coba tampilan baru <a href="https://s.klas.or.id/new">disini</a></li>
                <li class="nav-item"><a class="nav-link" {{ session()->get('locale') == 'en' || config('app.locale') == 'en' ? 'hidden' : '' }} href="{{ url('/en') }}">English</a><a class="nav-link" {{ session()->get('locale') == 'id' || config('app.locale') == 'id' ? 'hidden' : '' }} href="{{ url('/id') }}">Bahasa Indonesia</a></li>
            </ul>
        </div>
    </nav>
</header>
<main>
    <div class="container">
        <div class="row hide-on-med-down"><div class="col-12 text-center"><a href="https://klas.or.id"><img alt="KLAS LOGO" src="{{ asset('img/logo.png') }}" width="158px" height="100px"></a></div></div>
        <div class="row"><div class="col-12 text-center text-title"><h2>{{ __('SIMPLE AND FAST URL SHORTENER') }}</h2></div></div>
        <div class="row"><div class="col-12 text-center text-description"><p>{{ __('by Kelompok Linux Arek Suroboyo') }}</p></div></div>
        <div class="row"><div class="col-sm-12 col-md-12 col-lg-8 offset-sm-0 offset-md-0 offset-lg-2 text-left"><main style="margin-top: 0;">@yield('content')</main></div></div>
    </div>
</main>
<footer class="fixed-bottom card-footer hide-on-med-down">
    <div class="row text-center">
        <div class="col"><span class="text-muted"><a href="https://github.com/cangkrukan-klas/klas-url-shortener"><i class="fab fa-github fa-2x" style="color: black"></i></a></span></div>
        <div class="col">{{ __('Short link created') }}: {{ (\App\DataStatistik::query()->where('nama', 'shortlinkgenerate')->first()->nilai) + (\App\DataStatistik::query()->where('nama', 'shortlinkcustom')->first()->nilai) }} | Coba tampilan baru <a href="https://s.klas.or.id/new">disini</a></div>
        <div class="col"><a href="/en/" {{ session()->get('locale') == 'en' || config('app.locale') == 'en' ? 'hidden' : '' }}>en</a><a href="/id/" {{ session()->get('locale') == 'id' || config('app.locale') == 'id' ? 'hidden' : '' }}>id</a></div>
    </div>
</footer>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
@yield('jsscript')
</body>
</html>
