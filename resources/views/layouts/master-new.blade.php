<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="theme-color" content="#2a2c39"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ asset('img/android-icon-36x36.png') }}">
    <link rel="apple-touch-icon" href="{{ asset('img/android-icon-36x36.png') }}">
    @yield('klas-desc')
    @yield('klas-keywords')
    @yield('klas-og-title')
    @yield('klas-og-image')
    @yield('klas-og-description')
    @yield('klas-og-sitename')
    @yield('klas-title')


    @stack('head')

    <!--  Styles  -->
    <link rel="stylesheet" href="{{ mix('css/master.css') }}">
</head>

<body>
    <div id="app">
        <img alt="KLAS Logo" src="{{ asset('img/klas.webp') }}" id="imageLogo">
        <h1>{{ __('KLAS URL Shortener') }}</h1>
        <p>{{ __('Safe, Fast, and Free Forever') }}</p>
        @yield('content')
    </div>
    <!-- built files will be auto injected -->
    <footer class="relative-bottom card-footer hide-on-med-down bg-transparent border-0">
        <div class="row text-center">
            <div class="col">
                <span class="text-muted text-white">
                    <a href="https://github.com/cangkrukan-klas/klas-url-shortener">
                        <i class="fab fa-github fa-2x" style="color: white"></i>
                        <span hidden>GitHub</span>
                    </a>
                </span>
            </div>
            <div class="col">
                <span class="text-white">{{ __('Maintained by Kelompok Linux Arek Suroboyo') }}</span>
            </div>
            <div class="col"></div>
        </div>
    </footer>

    <!--  Javascript  -->
    <script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <script src="{{ mix('js/master.js') }}" async></script>
</body>

</html>
