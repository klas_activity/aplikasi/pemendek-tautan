<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>S.KLAS Admin Page</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" href="{{ asset('img/logo-32x32.png') }}" sizes="32x32">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="https://unpkg.com/ionicons@3.0.0/dist/css/ionicons.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="{{ mix('css/admin.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">
    <header class="main-header">
        <a href="" class="logo">
            <span class="logo-mini">KLAS</span>
            <span class="logo-lg">KLAS Admin</span>
        </a>
        <nav class="navbar navbar-static-top" role="navigation">
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <i class="fa fa-bars"></i>
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="{{ asset('img/logo-192x192.png') }}" class="user-image" alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{ \Auth::user()['name'] }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="{{ asset('img/logo-192x192.png') }}" class="img-circle" alt="User Image">

                                <p>
                                    {{ \Auth::user()['name'] }}
                                    <small>{{ \Auth::user()['email'] }}</small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-right">
                                    <a href="{{ url('logout') }}"
                                       class="btn btn-default btn-flat">{{ __('Logout') }}</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ asset('img/logo-192x192.png') }}" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{ \Auth::user()['name'] }}</p>
                    <!-- Status -->
                    <a href="#">{{ \Auth::user()['email'] }}</a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">{{ __('Menu') }}</li>
                <!-- Optionally, you can add icons to the links -->
                <li class="{{ (\Route::current()->getName() == 'admin.dashboard') ? 'active' : '' }}">
                    <a href="{{ route('admin.dashboard') }}"><i class="fa fa-tachometer-alt"></i>
                        <span>{{ __('Dashboard') }}</span></a>
                </li>
                <li class="{{ \Route::current()->getName() == 'admin.shorturl' ? 'active' : '' }}">
                    <a href="{{ route('admin.shorturl') }}"><i class="fa fa-table"></i>
                        <span>{{ __('Short URLs') }}</span></a>
                </li>
                <li class="{{ \Route::current()->getName() == 'admin.customurl' ? 'active' : '' }}">
                    <a href="{{ route('admin.customurl') }}"><i class="fa fa-table"></i>
                        <span>{{ __('Custom URLs') }}</span></a>
                </li>
                <li class="header">{{ __('Setting') }}</li>
                <li class="{{ session()->get('locale') == 'en' || config('app.locale') == 'en' ? 'active' : '' }}"><a
                            href="/en/?next=admin/dashboard"><i class="fa fa-language"></i> <span>English</span></a>
                </li>
                <li class="{{ session()->get('locale') == 'id' || config('app.locale') == 'id' ? 'active' : '' }}"><a
                            href="/id/?next=admin/dashboard"><i class="fa fa-language"></i>
                        <span>Bahasa Indonesia</span></a></li>
                <li><a href="{{ url('logout') }}"><i class="fa fa-sign-out-alt"></i> <span>{{ __('Logout') }}</span></a>
                </li>
            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <main>
        @yield('content')
    </main>

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">

        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2018 <a href="https://klas.or.id">Kelompok Linux Arek Suroboyo</a>.</strong> All rights
        reserved.
    </footer>
</div>
<!-- ./wrapper -->
<script src="https://code.jquery.com/jquery.min.js" crossorigin="anonymous"></script>
<!-- Bootstrap 3.3.7 -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"></script>
<!-- AdminLTE App -->
<script src="{{ mix('js/app.js') }}"></script>
@yield('jsscript')
</body>
</html>
