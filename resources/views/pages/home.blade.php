@extends('layouts.master')
@section('content')
    @if(isset($error))
        <div class="alert alert-danger alert-dismissible fade show justify-content-center">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>{{ __('Error!') }}</strong> {{ __($error) }}
        </div>
        @if(isset($customurl))
        <div class="alert alert-info alert-dismisible fade show">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Info! </strong>{{ __('This URL has been used with QR code as shown') }}
            <hr>
            <div class="container">
                <div class="row form-group">
                    <div class="col mx-auto text-center">
                    <img style="width: 100%; height: 100%; max-width: 200px; max-height: 200px;" 
                id="qrCodeImage" src="data:image/png;base64,{!! base64_encode( QrCode::format('png')->encoding('UTF-8')->size(600)->margin(1)->errorCorrection('H')->merge('/public/img/logo-klas.jpeg', .3, false)->generate("https://" . (env('APP_DOMAIN') ?? "s.klas.or.id") . "/" . $customurl) ) !!}" alt="">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col mx-auto text-center">
                        <button type="submit" class="btn btn-primary"
                            onclick="downloadImage()">{{ __('Unduh') }}</button>
                    </div>
                </div>
            </div>
        </div>
        @endif
        <div class="card shadow-sm">
            <div class="card-body bg-light text-center">{{ __('Make your short link here for free') }}
                <div class="row">
                    <div class="col mx-auto">
                        <button type="submit" class="btn btn-primary"
                                onclick="window.location = '{{ url('/') }}';">{{ __('Create Now') }}</button>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="card shadow-sm">
            <div class="card-body bg-light">
                <form method="POST" action="{{ route('doshort')  }}"
                      aria-label="{{ __('URL Shortener') }}">
                    @csrf
                    <div class="form-group"><label for="urlform">{{ __('URL') }}</label><input id="urlform" type="url"
                                                                                               class="form-control"
                                                                                               name="url"
                                                                                               value="{{ isset($url) ? $url : "" }}"
                                                                                               placeholder="https://"
                                                                                               aria-describedby="urlHelp"
                                                                                               required autofocus>
                        <small id="urlHelp"
                               class="form-text">{{ __('Enter the link that you want to short.') }}</small>
                    </div>
                    <div class="form-group"><label for="customurlform">{{ __('Custom URLs (Optional)') }}</label>
                        <div class="row justify-content-center" id="customurlform"
                             style="padding-left: 0; padding-right: 0;">
                            <div class="input-group col">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">https://s.klas.or.id/</div>
                                </div>
                                <input aria-label="" type="text" class="form-control" name="customurl" minlength="4"
                                       placeholder="{{ __("cangkruk'an-2019 (example)") }}"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-2 mx-auto">
                            <button type="submit" class="btn btn-primary" style="color: white">{{ __('Send') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endif
@endsection
@section('jsscript')
    @if(isset($error))
        <script>$('.alert').alert()
    function downloadImage() {
        var data = document.getElementById("qrCodeImage").getAttribute('src');
        var a = $("<a>")
            .attr("href", data)
            .attr("download", "qr-code.png")
            .appendTo("body");

        a[0].click();

        a.remove();
    }
</script>
    @endif
@endsection
