@extends('layouts.master-new')
@section('klas-title')
    <title>{{ __('URL Shortener by Kelompok Linux Arek Suroboyo') }}</title>
@endsection
@section('klas-desc')
    <meta name="description"
        content="{{ __('Alat Pemendek Tautan yang dibuat dan dirawat oleh KLAS (Kelompok Linux Arek Suroboyo).') }}" />
@endsection
@section('klas-keywords')
    <meta name="keywords" content="{{ __('klas, url-shortener') }}"/>
@endsection
@section('klas-og-title')
    <meta property="og:title" content="{{ __('URL Shortener by Kelompok Linux Arek Suroboyo') }}">
    <meta property="twitter:title" content="{{ __('URL Shortener by Kelompok Linux Arek Suroboyo') }}">
@endsection
@section('klas-og-description')
    <meta property="og:description" content="{{ __('Alat Pemendek Tautan yang dibuat dan dirawat oleh KLAS (Kelompok Linux Arek Suroboyo).') }}">
@endsection
@section('klas-og-sitename')
    <meta property="og:site_name" content="{{ __('Pemendek URL KLAS') }}">
    <meta property="twitter:site" content="{{ __('Pemendek URL KLAS') }}">
@endsection
@section('klas-og-image')
    <meta property="og:image" content="{{ asset('img/sklas2.png') }}">
    <meta property="twitter:image" content="{{ asset('img/sklas2.png') }}">
@endsection
@section('content')
    @if(isset($error))
        <div class="alert alert-danger alert-dismissible fade show justify-content-center">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>{{ __('Error!') }}</strong> {{ __($error) }}
        </div>
        <div class="card shadow-sm">
            <div class="card-body bg-dark justify-content-center">{{ __('Make your short link here for free') }}
                <div class="row mt-2">
                    <div class="col-3 mx-auto">
                        <button type="submit" class="btn btn-primary" style="padding: 10px 16px 10px 16px"
                                onclick="window.location = '{{ url('/') }}';">{{ __('Create Now') }}</button>
                    </div>
                </div>
            </div>
        </div>
    @else
    <div class="col-sm-12 col-md-12 col-lg-7 offset-sm-0 offset-md-0 offset-lg-2 mt-4 mx-auto text-left home">
        <label for="formShortUrl" hidden>{{ __('Long URL Form') }}</label>
        <form id="formShortUrl" aria-label="URL Shortener">
            <div class="col my-4">
                <div class="card card-body p-0 m-0">
                    <div class="input-group">
                        <input type="url" class="form-control" id="urlform" name="url"
                               placeholder="{{ __('Write your long URL here ...') }}" required>
                        <div class="input-group-prepend">
                            <button class="btn btn-light" type="button" aria-expanded="false" id="clearBtn">
                                <i class="fas fa-times d-block d-lg-none"></i>
                                <span class="d-none d-lg-block">{{ __('Reset') }}</span>
                            </button>
                            <button class="btn btn-light" type="button" data-toggle="collapse" data-target="#customInputCollapse"
                                aria-expanded="false" aria-controls="customInputCollapse">
                                <i class="fas fa-wrench d-block d-lg-none"></i>
                                <span class="d-none d-lg-block">{{ __('Customize') }}</span>
                            </button>
                            <button type="submit" id="sendBtn" class="btn pl-4 pr-4 pt-0 pb-0 font-weight-bold">
                                <i class="fas fa-plus d-block d-lg-none"></i>{{ __('Create') }}
                            </button>
                        </div>
                    </div>
                </div>
                <div class="collapse" id="customInputCollapse">
                    <div class="card card-body">
                        <div class="row m-0 p-0 mt-2">
                            <div class="col m-0 p-0">
                                <label for="passwordInput">{{ __('Custom Link') }}</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">https://s.klas.or.id/</div>
                                    </div>
                                    <input aria-label="Tautan Pendek Kustom" type="text" class="form-control" maxlength="25" name="customurl" id="customInput"
                                        minlength="4" placeholder="acara-keren (contoh)">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="collapse container" id="resultSection">
            <div class="card card-body m-0">
                <div class="row">
                    <div class="col m-0 align-self-center">
                        <p class="text-black-50 text-long-url m-0" id="textLongUrl">https://klas.or.id/</p>
                        <p class="text-short-url m-0"><a href="https://klas.or.id/" id="textShortUrl">https://klas.or.id/</a></p>
                        <button type="submit" class="btn btn-sm btn-block btn-outline-primary" id="copyBtn">
                            <i class="fa fa-clipboard pr-md-1"></i>
                            <span>{{ __('Copy') }}</span>
                        </button>
                    </div>
                    <div class="align-items-center col m-0">
                        <div class="row">
                            <div class="col m-0 align-items-center">
                                <img class="align-items-center" id="qrCodeImage" src="" alt="">
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col m-0 align-self-end" id="sosmedBtn">
                                <button type="submit" class="btn btn-sm btn-block btn-outline-primary" id="dloadBtn">
                                    <i class="fa fa-download pr-md-1"></i>
                                    <span>{{ __('Download QR') }}</span>
                                </button>
                                <a id="sfacebook" target="_blank" href="https://facebook.com" class="sosmed fa fa-facebook"></a>                                
                                <a id="stwitter" target="_blank" href="https://twitter.com" class="sosmed fa fa-twitter"></a>
                                <a id="slinkedin" target="_blank" href="https://linkedin.com" class="sosmed fa fa-linkedin"></a>
                                <a id="swhatsapp" target="_blank" href="https://whatsapp.com" class="sosmed fa fa-whatsapp"></a>
                                <a id="stelegram" target="_blank" href="https://telegram.org" class="sosmed fa fa-telegram"></a>
                            </div>           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection
@section('jsscript')
    @if(isset($error))
        <script>$('.alert').alert()</script>
    @endif
@endsection
