@extends('layouts.master-new')
@section('klas-title')
    @if(isset(($klasarr['title']))&& $klasarr['title'] !== "")
    <title> {{ $klasarr['title'] }}</title>
    @else
    <title>{{ __('URL Shortener by Kelompok Linux Arek Suroboyo') }}</title>
    @endif
@endsection
@section('klas-desc')
    @if(isset(($klasarr['description'])) && $klasarr['description'] !== "")
    <meta name="description"
        content="{{ $klasarr['description'] }}" />
    @elseif (isset(($klasarr['og:description'])) && $klasarr['og:description'] !== "")
    <meta name="description"
        content="{{ $klasarr['og:description'] }}" />
    @else
    <meta name="description"
        content="{{ __('Alat Pemendek Tautan yang dibuat dan dirawat oleh KLAS (Kelompok Linux Arek Suroboyo).') }}" />
     @endif
@endsection
@section('klas-keywords')
    @if(isset(($klasarr['keywords']))&& $klasarr['keywords'] !== "")
    <meta name="keywords" content="{{ $klasarr['keywords'] }}"/>
    @else
    <meta name="keywords" content="{{ __('klas, url-shortener') }}"/>
    @endif
@endsection
@section('klas-og-title')
    @if(isset(($klasarr['og:title']))&& $klasarr['og:title'] !== "")
    <meta property="og:title" content="{{ $klasarr['og:title'] }}">
    <meta property="twitter:title" content="{{ $klasarr['og:title'] }}">
    @else
    <meta property="og:title" content="{{ __('URL Shortener by Kelompok Linux Arek Suroboyo') }}">
    <meta property="twitter:title" content="{{ __('URL Shortener by Kelompok Linux Arek Suroboyo') }}">
    @endif
@endsection
@section('klas-og-description')
    @if(isset(($klasarr['og:description']))&& $klasarr['og:description'] !== "")
    <meta property="og:description" content="{{ $klasarr['og:description'] }}">
    @else
    <meta property="og:description" content="{{ __('Alat Pemendek Tautan yang dibuat dan dirawat oleh KLAS (Kelompok Linux Arek Suroboyo).') }}">
    @endif
@endsection
@section('klas-og-sitename')
    @if(isset(($klasarr['og:site_name']))&& $klasarr['og:site_name'] !== "")
    <meta property="og:site_name" content="{{ $klasarr['og:site_name'] }}">
    <meta property="twitter:site" content="{{ $klasarr['og:site_name'] }}">
    @else
    <meta property="og:site_name" content="{{ __('Pemendek URL KLAS') }}">
    <meta property="twitter:site" content="{{ __('Pemendek URL KLAS') }}">
    @endif
@endsection
@section('klas-og-image')
    @if(isset(($klasarr['og:image']))&& $klasarr['og:image'] !== "")
    <meta property="og:image" content="{{ $klasarr['og:image'] }}">
    <meta property="twitter:image" content="{{ $klasarr['og:image'] }}">
    @else
    <meta property="og:image" content="{{ asset('img/sklas2.png') }}">
    <meta property="twitter:image" content="{{ asset('img/sklas2.png') }}">
    @endif
@endsection

@push('head')
<meta http-equiv="refresh" content="3; url={{ $klasarr['klasurl'] }}">
@endpush

@section('content')
<div class="col-sm-12 col-md-12 col-lg-7 offset-sm-0 offset-md-0 offset-lg-2 mt-4 mx-auto home">
    <h3 class="text-center text-success">{{ __('Redirecting') }}</h3>
    <div class="lds-ellipsis">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>
@endsection
