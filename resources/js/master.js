const QRCode = require("qrcode");
const { createCanvas, loadImage } = require("canvas");
const shortLinkForm = document.getElementById("formShortUrl");
const customInput = document.getElementById("customInput");
const textLongUrl = document.getElementById("textLongUrl");
const textShortUrl = document.getElementById("textShortUrl");
const clearBtn = document.getElementById("clearBtn");
const copyBtn = document.getElementById("copyBtn");
const urlInput = document.getElementById("urlform");
const url = "/api/create";

/*social media buttons*/
const sfacebook = document.getElementById("sfacebook");
const stwitter = document.getElementById("stwitter");
const slinkedin = document.getElementById("slinkedin");
const swhatsapp = document.getElementById("swhatsapp");
const stelegram = document.getElementById("stelegram");

copyBtn.addEventListener("click", copyToClipboard);
dloadBtn.addEventListener("click", downloadImage);

$("#urlform").blur(function() {
    setTimeout(function() {
        if (!$(document.activeElement).is("#urlform")) {
            $(window).scrollTop(0, 0);
        }
    }, 0);
});

shortLinkForm.addEventListener("submit", handleFormSubmit);
clearBtn.addEventListener("click", clearText);

function clearText() {
    urlInput.value = "";
}

async function showShortedLink(responseData) {
    var longUrl = responseData.url;
    var shortUrl = responseData.shorturl;
    if (responseData.customurl !== '') {
        shortUrl = responseData.customurl;
    }
    textLongUrl.innerText = longUrl;
    textShortUrl.innerText = "https://s.klas.or.id/" + shortUrl;
    textShortUrl.href = "https://s.klas.or.id/" + shortUrl;
    $("#resultSection").collapse("show");
    $("#customInputCollapse").collapse("hide");

    /*Invoke social media share button URLs*/
    sfacebook.href =
        "https://www.facebook.com/sharer.php?u=" + textShortUrl.href;
    stwitter.href =
        "https://twitter.com/share?url=" +
        textShortUrl.href +
        "&text=Pemendek Tautan KLAS";
    slinkedin.href =
        "https://www.linkedin.com/shareArticle?url=" +
        textShortUrl.href +
        "&title=Pemendek Tautan KLAS";
    swhatsapp.href =
        "https://api.whatsapp.com/send?text=Pemendek Tautan KLAS " +
        textShortUrl.href;
    stelegram.href =
        "https://t.me/share?url=" +
        textShortUrl.href +
        "&text=Pemendek Tautan KLAS";

    /*Generating QR code*/
    const qrCode = await createQR(
        textShortUrl.href,
        "./img/sklas2.png",
        600,
        200
    );
    var img = document.getElementById("qrCodeImage");
    img.src = qrCode;
    /*resetting input value*/
    customInput.value = "";
}

async function createQR(dataForQRcode, center_image, width, cwidth) {
    const canvas = createCanvas(width, width);
    QRCode.toCanvas(canvas, dataForQRcode, {
        errorCorrectionLevel: "H",
        margin: 1,
        width: width,
        color: {
            dark: "#000000FF",
            light: "#FFFFFFFF"
        }
    });
    const ctx = canvas.getContext("2d");
    const img = await loadImage(center_image);
    const center = (width - cwidth) / 2;
    ctx.drawImage(img, center, center, cwidth, cwidth);
    return canvas.toDataURL("image/png");
}

async function handleFormSubmit(event) {
    event.preventDefault();
    $("#resultSection").collapse("hide");
    $("#urlform").blur();
    const form = event.currentTarget;
    const token = $('meta[name="csrf-token"]').attr("content");
    try {
        const formData = new FormData(form);
        const responseData = await postFormDataAsJson({ url, formData, token });
        /*TODO: Check for duplicate custom URL*/
        showShortedLink(responseData.data);
    } catch (error) {
        alert(error);
    }
}

async function postFormDataAsJson({ url, formData, token }) {
    const plainFormData = Object.fromEntries(formData.entries());
    const formDataJsonString = JSON.stringify(plainFormData);

    const fetchOptions = {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
            "X-CSRF-TOKEN": token
        },
        body: formDataJsonString
    };

    const response = await fetch(url, fetchOptions);

    if (!response.ok) {
        const errorMessage = await response.text();
        throw new Error(errorMessage);
    }

    return response.json();
}

function copyToClipboard() {
    var copyText = document.getElementById("textShortUrl").text;
    document.addEventListener(
        "copy",
        function(event) {
            event.clipboardData.setData("text/plain", copyText);
            event.preventDefault();
            document.removeEventListener("copy", function() {}, true);
        },
        true
    );
    document.execCommand("copy");
    /* Alert the copied text */
    alert("Alamat berhasil disalin!");
    textLongUrl.blur();
}

function downloadImage() {
    var data = document.getElementById("qrCodeImage").getAttribute("src");
    var a = $("<a>")
        .attr("href", data)
        .attr("download", "qr-code.png")
        .appendTo("body");

    a[0].click();

    a.remove();
}
